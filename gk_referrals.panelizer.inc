<?php
/**
 * @file
 * gk_referrals.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_referrals_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_referrals_referrals';
  $panelizer->title = 'Referrals: Referrals';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '3a41a166-4d9e-422c-ad8f-cb3e35ff0883';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e4787b34-696b-4f84-8a9f-18a4dbd53164';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e4787b34-696b-4f84-8a9f-18a4dbd53164';
    $display->content['new-e4787b34-696b-4f84-8a9f-18a4dbd53164'] = $pane;
    $display->panels['content'][0] = 'new-e4787b34-696b-4f84-8a9f-18a4dbd53164';
    $pane = new stdClass();
    $pane->pid = 'new-e048167e-8355-43d4-8665-3d81b8ba3969';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_rewards-gk_rewards_tiers';
    $pane->shown = TRUE;
    $pane->access = '';
    $pane->configuration = '';
    $pane->cache = '';
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = '';
    $pane->extras = '';
    $pane->position = 1;
    $pane->locks = '';
    $pane->uuid = 'e048167e-8355-43d4-8665-3d81b8ba3969';
    $display->content['new-e048167e-8355-43d4-8665-3d81b8ba3969'] = $pane;
    $display->panels['content'][1] = 'new-e048167e-8355-43d4-8665-3d81b8ba3969';
    $pane = new stdClass();
    $pane->pid = 'new-7e9cbd4e-0b84-48c5-a0dd-3f706d91446c';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_referrals-gk_referrals_share';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '7e9cbd4e-0b84-48c5-a0dd-3f706d91446c';
    $display->content['new-7e9cbd4e-0b84-48c5-a0dd-3f706d91446c'] = $pane;
    $display->panels['content'][2] = 'new-7e9cbd4e-0b84-48c5-a0dd-3f706d91446c';
    $pane = new stdClass();
    $pane->pid = 'new-a59c158d-e738-4935-8721-dbf9d4fb08e0';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_referrals-gk_referrals_refer_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'a59c158d-e738-4935-8721-dbf9d4fb08e0';
    $display->content['new-a59c158d-e738-4935-8721-dbf9d4fb08e0'] = $pane;
    $display->panels['content'][3] = 'new-a59c158d-e738-4935-8721-dbf9d4fb08e0';
    $pane = new stdClass();
    $pane->pid = 'new-1a2d1ec3-748a-40d8-a427-1984d22d7fbd';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_referrals-gk_referrals_user_referrals';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '1a2d1ec3-748a-40d8-a427-1984d22d7fbd';
    $display->content['new-1a2d1ec3-748a-40d8-a427-1984d22d7fbd'] = $pane;
    $display->panels['content'][4] = 'new-1a2d1ec3-748a-40d8-a427-1984d22d7fbd';
    $pane = new stdClass();
    $pane->pid = 'new-0281d259-bbcd-4ab1-8faf-39d2b9d88066';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0281d259-bbcd-4ab1-8faf-39d2b9d88066';
    $display->content['new-0281d259-bbcd-4ab1-8faf-39d2b9d88066'] = $pane;
    $display->panels['content_header'][0] = 'new-0281d259-bbcd-4ab1-8faf-39d2b9d88066';
    $pane = new stdClass();
    $pane->pid = 'new-efadcee8-0b3f-456c-adfb-6acd61b8c698';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-members-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'efadcee8-0b3f-456c-adfb-6acd61b8c698';
    $display->content['new-efadcee8-0b3f-456c-adfb-6acd61b8c698'] = $pane;
    $display->panels['tertiary'][0] = 'new-efadcee8-0b3f-456c-adfb-6acd61b8c698';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_referrals_referrals'] = $panelizer;

  return $export;
}
