<?php
/**
 * @file
 * gk_referrals.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_referrals_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}
