<?php

class GKReferralsGKCSVExportBuildHandler extends GKCSVExportBuildHandler {
  protected static $name = 'GK Referrals (GK CSV)';

  public function __construct($options = array()) {
    parent::__construct($options);
  }

  /**
   * Implements ExportBuildHandler::getData().
   */
  public function getData() {
    // Build a query that retrieves all referrals.
    $query = db_select('gk_referrals', 'r')->fields('r');

    // Join the users table to get the referrer's uid and email address
    $query->join('users', 'u', 'r.uid = u.uid');

    $query->addField('u', 'uid', 'referrer_uid');
    $query->addField('u', 'mail', 'referrer_mail');

    // We only want referral records that have been confirmed
    $query->condition('r.status', '1');

    // Date restrictions
    if (!empty($this->options['date_from'])) {
      $query->condition('r.created', strtotime($this->options['date_from']), '>=');
    }

    if (!empty($this->options['date_to'])) {
      $query->condition('r.created', strtotime($this->options['date_to']), '<=');
    }

    // Run the query
    $data = $query->execute()->fetchAll();

    // Massage the data.
    foreach ($data as $datum) {
      // Set the 'event' field value.
      $datum->event = 'referral-by-' . $datum->referrer_uid;

      // If the first/last name fields are blank for this referral then the DB
      // will hold a NULL value instead of the empty string. We should replace
      // these with the empty string because the ExportFieldHandler class does
      // not like NULL values. @see ExportFieldHandler::__construct()
      if (is_null($datum->first_name)) {
        $datum->first_name = '';
      }

      if (is_null($datum->last_name)) {
        $datum->last_name = '';
      }
    }

    return self::csvBuildRows(self::csvHeaderInfo(), $data);
  }

  /**
   * Overrides GKCSVExportBuildHandler::csvHeaderInfo().
   */
  public static function csvHeaderInfo() {
    return array_merge(parent::csvHeaderInfo(), array(
      'event' => array(
        'real field' => 'event',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'mail' => array(
        'real field' => 'referrer_mail',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'referee_mail' => array(
        'real field' => 'mail',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'referee_first_name' => array(
        'real field' => 'first_name',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'referee_last_name' => array(
        'real field' => 'last_name',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'created' => array(
        'real field' => 'created',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'changed' => array(
        'real field' => 'changed',
        'handler' => 'DefaultExportFieldHandler',
      ),
    ));
  }
}
